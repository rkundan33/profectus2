<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>{{ config('app.name', 'Profectus') }}</title>
    <meta name="description" content="Profectus">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500;600;700;800;900&display=swap"
        rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ asset('') }}assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{ asset('') }}assets/css/slick.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css">
    {{-- <link rel="stylesheet" href="{{ asset('') }}assets/css/font-awesome.min.css"> --}}
    <link rel="stylesheet" href="{{ asset('') }}assets/css/main.css">
    @yield('styles')

</head>

<body>
    <div class="loaderWrap"><img src="{{ asset('') }}assets/images/preload.gif"></div>
    <div class="headTop row p-2 pl-5">
        <div class="col-md-6">
            <a>Support@profectus.com</a>
        </div>
        <div class="col-md-6 topnav text-right pr-4">
            <a>About Us</a>
            <a>Contact Us</a>
        </div>
    </div>
    <div class="logoWrap text-center">
        <img src="{{ asset('') }}assets/images/logow.png">
    </div>
    <div class="container mb-5">
        <div class="col-md-12 col-12 p-0">
            <nav class="navbar navbar-expand-md">
                <div class="collapse navbar-collapse" id="collapsibleNavbar">
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link search" id="searchbtn">
                                <i class="fa fa-search" aria-hidden="true"></i>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="index.html"> <span>Awards</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#about"> <span>Fun Facts</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#whatisbarter"><span>Crypto</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#Benefits"><span>Health</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#WhyUS"><span>News </span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#whtds"><span> Fashion </span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#reachus"><span> Entreprenur Stories </span></a>
                        </li>
                    </ul>
                </div>
            </nav>
            <div class="searchWrap">
                <input type="search" class="form-control menusearch">
                <input type="submit" class="search-submit" value="Search">
            </div>
        </div>
    </div>


    @yield('content')

    <div class="footer">
        <!-- Site footer -->
        <footer class="site-footer">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-md-6">
                        <h6>About</h6>
                        <p class="text-justify">Scanfcode.com <i>CODE WANTS TO BE SIMPLE </i> is an initiative to
                            help the upcoming programmers with the code. Scanfcode focuses on providing the most
                            efficient code or snippets as the code wants to be simple. We will help programmers build up
                            concepts in different programming languages that include C, C++, Java, HTML, CSS, Bootstrap,
                            JavaScript, PHP, Android, SQL and Algorithm.</p>
                    </div>
                    <div class="col-xs-6 col-md-3">
                        <h6>Categories</h6>
                        <ul class="footer-links">
                            <li><a href="http://scanfcode.com/category/c-language/">C</a></li>
                            <li><a href="http://scanfcode.com/category/front-end-development/">Latest News</a></li>
                            <li><a href="http://scanfcode.com/category/back-end-development/">Events</a></li>
                            <li><a href="http://scanfcode.com/category/java-programming-language/">Upcoming</a></li>
                            <li><a href="http://scanfcode.com/category/android/">Blog</a></li>
                            <li><a href="http://scanfcode.com/category/templates/">Trending</a></li>
                        </ul>
                    </div>
                    <div class="col-xs-6 col-md-3">
                        <h6>Quick Links</h6>
                        <ul class="footer-links">
                            <li><a href="http://scanfcode.com/about/">About Us</a></li>
                            <li><a href="http://scanfcode.com/contact/">Contact Us</a></li>
                            <li><a href="http://scanfcode.com/contribute-at-scanfcode/">Contribute</a></li>
                            <li><a href="http://scanfcode.com/privacy-policy/">Privacy Policy</a></li>
                            <li><a href="http://scanfcode.com/sitemap/">Sitemap</a></li>
                        </ul>
                    </div>
                </div>
                <hr>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-sm-6 col-xs-12">
                        <p class="copyright-text">Copyright &copy; 2017 All Rights Reserved by
                            <a href="#">Profectus</a>.
                        </p>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <ul class="social-icons">
                            <li><a class="facebook" href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a class="twitter" href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a class="dribbble" href="#"><i class="fa fa-dribbble"></i></a></li>
                            <li><a class="linkedin" href="#"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </footer>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js" type="text/javascript"></script>
    <script src="{{ asset('') }}assets/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="{{ asset('') }}assets/js/slick.js" type="text/javascript"></script>

    <script>
        $('.lazy').slick({
            dots: false,
            arrow: true,
            infinite: true,
            fade: true,
            speed: 1500,
            slidesToShow: 1,
            slidesToScroll: 1
        });


        if ($('.slick-slide').hasClass('slick-active')) {
            $('.sliderTxt').addClass('animated fadeInUp');
        } else {
            $('.sliderTxt').removeClass('animated fadeInUp');
        }

        $(".lazy").on("beforeChange", function() {

            $('.sliderTxt').removeClass('animated fadeInUp').hide();
            setTimeout(() => {
                $('.sliderTxt').addClass('animated fadeInUp').show();

            }, 1000);

        })

        $("#searchbtn").click(function() {
            $(".searchWrap").slideToggle("slow");
            $(".searchWrap").css('display', 'flex')
        });

        $(".clientsWrap").slick({
            speed: 10000,
            autoplay: true,
            autoplaySpeed: 0,
            cssEase: 'linear',
            slidesToShow: 5,
            slidesToScroll: 1,
            infinite: true,
            swipeToSlide: true,
            centerMode: true,
            focusOnSelect: true,
            arrows: false,
            responsive: [{
                    breakpoint: 750,
                    settings: {
                        slidesToShow: 3,
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                    }
                }
            ]
        });

        $('body').css('overflow-y', 'hidden');
        setTimeout(function() {
            $('.loaderWrap').css('display', 'none');

        }, 3000);
        $(document).ready(function() {
            setTimeout(function() {
                $('.loaderWrap').css('display', 'flex');
                $('body').css('overflow-y', 'scroll');
            }, 2000);
        });
    </script>
    @yield('scripts')
</body>

</html>
