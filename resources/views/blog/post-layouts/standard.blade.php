@extends('layout.main')

@section('content')

<div class="container">
    <div class="row">
      <div class="col-md-8">
        <div class="col-md-12 blogDetailsWrap">
          <h2 class="blogHead">Contrary to popular belief, Lorem Ipsum is not simply random text.</h2>
          <p>By : <span>{{$post->author}}</span>  &nbsp; / &nbsp; {{$post->published_at}}  &nbsp;/ &nbsp; <span>Technology</span></p>
          <div class="blogImg">
            <img src="{{$post->post_image}}">
          </div>
          <div class="blogpara">
                {!! $post->content_html !!}
          </div>
        </div>
      </div>
      <div class="col-md-4 postdiv">
        <div class="RecentPost">
          <h3>Recent Blogs</h3>

          <ul class="recentList">
            <li>
              <h4>Learn about disabilities through movies and TV shows</h4>
              <p><span>Entertainment</span>  OCTOBER 17, 2021</p>
            </li>
            <li>
              <h4>Learn about disabilities through movies and TV shows</h4>
              <p><span>Entertainment</span>  OCTOBER 17, 2021</p>
            </li>
            <li>
              <h4>Learn about disabilities through movies and TV shows</h4>
              <p><span>Entertainment</span>  OCTOBER 17, 2021</p>
            </li>
            <li>
              <h4>Learn about disabilities through movies and TV shows</h4>
              <p><span>Entertainment</span>  OCTOBER 17, 2021</p>
            </li>

            <li>
              <img src="{{ asset('') }}assets/images/wb1.jpg">
            </li>
            <li>
              <img src="{{ asset('') }}assets/images/wb2.jpg">
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>


  <div class="clientsWrap mt-5 row">
          <div>
              <img src="{{ asset('') }}assets/images/airtel.jpg">
          </div>
          <div>
              <img src="{{ asset('') }}assets/images/voda.jpg">
          </div>
          <div>
              <img src="{{ asset('') }}assets/images/rel.jpg">
          </div>
          <div>
              <img src="{{ asset('') }}assets/images/wip.jpg">
          </div>
          <div>
              <img src="{{ asset('') }}assets/images/tata.jpg">
          </div>
          <div>
              <img src="{{ asset('') }}assets/images/airtel.jpg">
          </div>
      </div>
  </div>
@endsection
