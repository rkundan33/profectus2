@extends("layout.main")


@section('content')
    <div class="bannerWrap">
        <div class="col-md-12 col-12 p-0">
            <div class="lazy">
                <div class="animate">
                    <div class="slickslider animate">
                        <div class="overlaywrap"></div>
                        <div class="SliderimgWrap">
                            <img
                                src="https://storage.googleapis.com/gweb-uniblog-publish-prod/images/Mayes_County-Oklahoma_DC52.max-1000x1000.jpg">
                        </div>
                        <div class="sliderTxt">
                            <h2>Welcome to Agra</h2>
                            <p>lorem ipsum dummy text</p>
                        </div>
                    </div>
                </div>
                <div class="animate">
                    <div class="slickslider animate">
                        <div class="SliderimgWrap">
                            <img
                                src="https://storage.googleapis.com/gweb-uniblog-publish-prod/original_images/sustainability_geo_hero.gif">
                        </div>
                        <div class="sliderTxt">
                            <h2>Welcome to Agra</h2>
                            <p>lorem ipsum dummy text</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="cardwrap row mt-4">
            <div class="col-md-3">
                <a href="{{url('')}}/blog-details" target="_blank">
                    <div class="card shadow">
                        <div class="cardimg">
                            <img src="{{ asset('') }}assets/images/wb3.jpg">
                        </div>
                        <div class="cardInfo mt-3">
                            <strong class="greenTxt">Lorem Ipsum</strong>
                            <h5>India top businee - Lorem ipsum</h5>
                        </div>
                        <div class="cardData">
                            <p>MAR 2021 . 12 MINS READ . 1462 VIEWS</p>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-3">
                <a href="Blogdetails.html" target="_blank">
                    <div class="card shadow">
                        <div class="cardimg">
                            <img src="{{ asset('') }}assets/images/wb3.jpg">
                        </div>
                        <div class="cardInfo mt-3">
                            <strong class="greenTxt">Lorem Ipsum</strong>
                            <h5>India top businee - Lorem ipsum</h5>
                        </div>
                        <div class="cardData">
                            <p>MAR 2021 . 12 MINS READ . 1462 VIEWS</p>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-3">
                <div class="card shadow">
                    <div class="cardimg">
                        <img src="{{ asset('') }}assets/images/wb3.jpg">
                    </div>
                    <div class="cardInfo mt-3">
                        <strong class="greenTxt">Lorem Ipsum</strong>
                        <h5>India top businee - Lorem ipsum</h5>
                    </div>
                    <div class="cardData">
                        <p>MAR 2021 . 12 MINS READ . 1462 VIEWS</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card shadow">
                    <div class="cardimg">
                        <img src="{{ asset('') }}assets/images/wb3.jpg">
                    </div>
                    <div class="cardInfo mt-3">
                        <strong class="greenTxt">Lorem Ipsum</strong>
                        <h5>India top businee - Lorem ipsum</h5>
                    </div>
                    <div class="cardData">
                        <p>MAR 2021 . 12 MINS READ . 1462 VIEWS</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card shadow">
                    <div class="cardimg">
                        <img src="{{ asset('') }}assets/images/wb3.jpg">
                    </div>
                    <div class="cardInfo mt-3">
                        <strong class="greenTxt">Lorem Ipsum</strong>
                        <h5>India top businee - Lorem ipsum</h5>
                    </div>
                    <div class="cardData">
                        <p>MAR 2021 . 12 MINS READ . 1462 VIEWS</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card shadow">
                    <div class="cardimg">
                        <img src="{{ asset('') }}assets/images/wb3.jpg">
                    </div>
                    <div class="cardInfo mt-3">
                        <strong class="greenTxt">Lorem Ipsum</strong>
                        <h5>India top businee - Lorem ipsum</h5>
                    </div>
                    <div class="cardData">
                        <p>MAR 2021 . 12 MINS READ . 1462 VIEWS</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card shadow">
                    <div class="cardimg">
                        <img src="{{ asset('') }}assets/images/wb3.jpg">
                    </div>
                    <div class="cardInfo mt-3">
                        <strong class="greenTxt">Lorem Ipsum</strong>
                        <h5>India top businee - Lorem ipsum</h5>
                    </div>
                    <div class="cardData">
                        <p>MAR 2021 . 12 MINS READ . 1462 VIEWS</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card shadow">
                    <div class="cardimg">
                        <img src="{{ asset('') }}assets/images/wb3.jpg">
                    </div>
                    <div class="cardInfo mt-3">
                        <strong class="greenTxt">Lorem Ipsum</strong>
                        <h5>India top businee - Lorem ipsum</h5>
                    </div>
                    <div class="cardData">
                        <p>MAR 2021 . 12 MINS READ . 1462 VIEWS</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="cardwrap row mt-4">
            <div class="col-md-12 text-center">
                <h3 class="pt-5 pb-0 textyellow">FUN FACTS</h3>
            </div>
            <div class="col-md-3">
                <div class="card shadow">
                    <div class="cardimg">
                        <img src="{{ asset('') }}assets/images/wb2.jpg">
                    </div>
                    <div class="cardInfo mt-3">
                        <strong class="greenTxt">Lorem Ipsum</strong>
                        <h5>India top businee - Lorem ipsum</h5>
                    </div>
                    <div class="cardData">
                        <p>MAR 2021 . 12 MINS READ . 1462 VIEWS</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card shadow">
                    <div class="cardimg">
                        <img src="{{ asset('') }}assets/images/wb1.jpg">
                    </div>
                    <div class="cardInfo mt-3">
                        <strong class="greenTxt">Lorem Ipsum</strong>
                        <h5>India top businee - Lorem ipsum</h5>
                    </div>
                    <div class="cardData">
                        <p>MAR 2021 . 12 MINS READ . 1462 VIEWS</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card shadow">
                    <div class="cardimg">
                        <img src="{{ asset('') }}assets/images/wb4.jpg">
                    </div>
                    <div class="cardInfo mt-3">
                        <strong class="greenTxt">Lorem Ipsum</strong>
                        <h5>India top businee - Lorem ipsum</h5>
                    </div>
                    <div class="cardData">
                        <p>MAR 2021 . 12 MINS READ . 1462 VIEWS</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card shadow">
                    <div class="cardimg">
                        <img src="{{ asset('') }}assets/images/wb3.jpg">
                    </div>
                    <div class="cardInfo mt-3">
                        <strong class="greenTxt">Lorem Ipsum</strong>
                        <h5>India top businee - Lorem ipsum</h5>
                    </div>
                    <div class="cardData">
                        <p>MAR 2021 . 12 MINS READ . 1462 VIEWS</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="entstoryWrap">
        <h3>Entreprenur Story</h3>
    </div>
    <div class="container">
        <div class="cardwrap wrap_ent row">
            <div class="col-md-3">
                <div class="card shadow">
                    <div class="cardimg">
                        <img src="{{ asset('') }}assets/images/wb2.jpg">
                    </div>
                    <div class="cardInfo mt-3">
                        <strong class="greenTxt">Lorem Ipsum</strong>
                        <h5>India top businee - Lorem ipsum</h5>
                    </div>
                    <div class="cardData">
                        <p>MAR 2021 . 12 MINS READ . 1462 VIEWS</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card shadow">
                    <div class="cardimg">
                        <img src="{{ asset('') }}assets/images/wb1.jpg">
                    </div>
                    <div class="cardInfo mt-3">
                        <strong class="greenTxt">Lorem Ipsum</strong>
                        <h5>India top businee - Lorem ipsum</h5>
                    </div>
                    <div class="cardData">
                        <p>MAR 2021 . 12 MINS READ . 1462 VIEWS</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card shadow">
                    <div class="cardimg">
                        <img src="{{ asset('') }}assets/images/wb4.jpg">
                    </div>
                    <div class="cardInfo mt-3">
                        <strong class="greenTxt">Lorem Ipsum</strong>
                        <h5>India top businee - Lorem ipsum</h5>
                    </div>
                    <div class="cardData">
                        <p>MAR 2021 . 12 MINS READ . 1462 VIEWS</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card shadow">
                    <div class="cardimg">
                        <img src="{{ asset('') }}assets/images/wb3.jpg">
                    </div>
                    <div class="cardInfo mt-3">
                        <strong class="greenTxt">Lorem Ipsum</strong>
                        <h5>India top businee - Lorem ipsum</h5>
                    </div>
                    <div class="cardData">
                        <p>MAR 2021 . 12 MINS READ . 1462 VIEWS</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-md-8">
                <div class="add1">
                    <img src="{{ asset('') }}assets/images/add.jpg">
                </div>
            </div>
            <div class="col-md-4">
                <div class="add1">
                    <img src="{{ asset('') }}assets/images/wb3.jpg">
                </div>
            </div>
        </div>
    </div>
    <div class="newswrap">
        <h3>NEWS AND UPDATES</h3>
    </div>
    <div class="container">
        <div class="cardwrap wrap_ent row">
            <div class="col-md-3">
                <div class="card shadow">
                    <div class="cardimg">
                        <img src="{{ asset('') }}assets/images/wb2.jpg">
                    </div>
                    <div class="cardInfo mt-3">
                        <strong class="greenTxt">Lorem Ipsum</strong>
                        <h5>India top businee - Lorem ipsum</h5>
                    </div>
                    <div class="cardData">
                        <p>MAR 2021 . 12 MINS READ . 1462 VIEWS</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card shadow">
                    <div class="cardimg">
                        <img src="{{ asset('') }}assets/images/wb1.jpg">
                    </div>
                    <div class="cardInfo mt-3">
                        <strong class="greenTxt">Lorem Ipsum</strong>
                        <h5>India top businee - Lorem ipsum</h5>
                    </div>
                    <div class="cardData">
                        <p>MAR 2021 . 12 MINS READ . 1462 VIEWS</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card shadow">
                    <div class="cardimg">
                        <img src="{{ asset('') }}assets/images/wb4.jpg">
                    </div>
                    <div class="cardInfo mt-3">
                        <strong class="greenTxt">Lorem Ipsum</strong>
                        <h5>India top businee - Lorem ipsum</h5>
                    </div>
                    <div class="cardData">
                        <p>MAR 2021 . 12 MINS READ . 1462 VIEWS</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card shadow">
                    <div class="cardimg">
                        <img src="{{ asset('') }}assets/images/wb3.jpg">
                    </div>
                    <div class="cardInfo mt-3">
                        <strong class="greenTxt">Lorem Ipsum</strong>
                        <h5>India top businee - Lorem ipsum</h5>
                    </div>
                    <div class="cardData">
                        <p>MAR 2021 . 12 MINS READ . 1462 VIEWS</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="clientsWrap mt-5 row">
            <div>
                <img src="{{ asset('') }}assets/images/airtel.jpg">
            </div>
            <div>
                <img src="{{ asset('') }}assets/images/voda.jpg">
            </div>
            <div>
                <img src="{{ asset('') }}assets/images/rel.jpg">
            </div>
            <div>
                <img src="{{ asset('') }}assets/images/wip.jpg">
            </div>
            <div>
                <img src="{{ asset('') }}assets/images/tata.jpg">
            </div>
            <div>
                <img src="{{ asset('') }}assets/images/airtel.jpg">
            </div>
        </div>
    </div>
@endsection
